define([
    'backbone',
    'backbone.marionette',
    './template',
    './Service'
], function (Backbone, Marionette, template, CartService) {
    return Marionette.ItemView.extend({
        template: template,

        ui: {
            input: 'input',
            email: 'form input[name=email]',
            phone: 'form input[name=number]'
        },

        events: {
            'click button[data-action=recalculate]': 'recalculate',
            'click button[data-action=order]': 'order'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        _updateCart: function () {
            this.ui.input.each(function () {
                CartService.add(parseInt(this.dataset.id), parseInt(this.value) || 0);
            });
        },

        recalculate: function () {
            this._updateCart();
            this.model.fetch({
                data: CartService._data
            })
        },

        order: function () {
            var email = this.ui.email.val();
            var phone = this.ui.phone.val();

            if(!email || !phone) {
                this.ui.email.css('outline', '2px solid #D01818');
                this.ui.phone.css('outline', '2px solid #D01818');
            }

            if (email && phone) {
                this.ui.email.css('outline', '2px solid transparent');
                this.ui.phone.css('outline', '2px solid transparent');

                    this._updateCart();
                    this.model.save({
                        email: email,
                        phone: phone,
                        cart: CartService._data
                    }).done(function (data) {
                        CartService.removeAll();
                        Backbone.history.navigate('order/' + data.orderId, {trigger: true, replace: true});
                    });

            }
        }
    });
});