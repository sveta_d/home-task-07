define([
    'backbone',
    'backbone.marionette',
    './template'
], function (Backbone, Marionette, template) {

    var View = Marionette.ItemView.extend({
        template: template
    });

    return Backbone.Router.extend({

        initialize: function (options) {
            this.container = options.container;
        },

        routes: {
            'order/:orderId': 'order'
        },

        order: function (orderId) {
            var model = new Backbone.Model;
            model.fetch({url: '/order/' + orderId}).done(function () {
                var view = new View({
                    model: model
                });
                this.container.show(view);
            }.bind(this));
        }
    });
});