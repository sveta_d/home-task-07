define([
    'backbone.marionette',
    './item-template'
], function (Marionette, template) {
    return Marionette.ItemView.extend({
        tagName: 'tr',
        className: 'product',
        template: template,

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        events: {
            'click button': 'toggle'
        },

        toggle: function () {
            this.model.toggle();
        }
    });
});