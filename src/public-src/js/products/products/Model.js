define([
    'backbone',
    '../../cart/Service'
], function (Backbone, CartService) {
    return Backbone.Model.extend({
        constructor: function (attributes, options) {
            attributes.added = CartService.has(attributes.id);
            return Backbone.Model.call(this, attributes, options);
        },

        toggle: function () {
            CartService.toggle(this.get('id'));
            this.set('added', CartService.has(this.get('id')));
        }
    });
});