define([
    'backbone.marionette',
    './template'
], function (Marionette, template) {
    return Marionette.ItemView.extend({
        tagName: 'a',
        attributes: {
            href: '#cart'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        render: function () {
            this.$el.html(template(this.model.toJSON()));
            return this;
        }
    });
});